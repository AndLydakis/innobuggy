//
// Created by lydakis on 25/06/18.
//
#include "odometry.cpp"

int main(int argc, char **argv) {

    ros::init(argc, argv, "odometry_test_node");

    int type;
    int amt;

    ROS_INFO("Starting the odometry node\n");
    differential_odometry odometry_feedback;

    while (!odometry_feedback.has_read_encoders) {
        ros::spinOnce();
    }

    if (!ros::param::get("~type", type))type = 1;
    if (!ros::param::get("~amt", amt))amt = 0;
    ROS_INFO("Starting left encoder value: %f ticks", odometry_feedback.getEncoder(0));
    ROS_INFO("Starting right encoder value: %f ticks", odometry_feedback.getEncoder(1));
//    odometry_feedback.straightLine();
//    odometry_feedback.onPointTurn(45);
    ROS_INFO("Movement type: %d, movement amt: %d", type, amt);
    if (type == 1)odometry_feedback.straightLine(amt);
    else odometry_feedback.onPointTurn(amt);

    return 0;
}

