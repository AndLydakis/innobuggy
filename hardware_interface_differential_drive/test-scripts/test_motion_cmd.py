#!/usr/bin/env python

import rospy

from std_msgs.msg import Int16MultiArray
from std_msgs.msg import MultiArrayLayout
from std_msgs.msg import MultiArrayDimension

# msg: [frontLeft(pwm, yaw), frontRight(pwm, yaw)]

pub = rospy.Publisher('motion_cmd', Int16MultiArray, queue_size=10)

rospy.init_node('publish_motion_cmd')

rate = int(rospy.get_param("rate", 5))
r = rospy.Rate(rate)

print "rate ", rate 

while not rospy.is_shutdown():

    # compose the multiarray message
    pwmVelocities = Int16MultiArray()

    myLayout = MultiArrayLayout()

    myMultiArrayDimension = MultiArrayDimension()

    myMultiArrayDimension.label = "motion_cmd"
    myMultiArrayDimension.size = 1
    myMultiArrayDimension.stride = 2

    myLayout.dim = [myMultiArrayDimension]
    myLayout.data_offset = 0

    pwmVelocities.layout = myLayout
    pwmVelocities.data = [60, 60]

    # publish the message and log in terminal
    pub.publish(pwmVelocities)
    rospy.loginfo("I'm publishing: [%d, %d]" % (pwmVelocities.data[0], pwmVelocities.data[1]))

    # repeat
    r.sleep()
