#ifndef _ODOMETRY_H_
#define _ODOMETRY_H_

#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/Int16MultiArray.h>
#include <geometry_msgs/Quaternion.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_broadcaster.h>

#include <ros/ros.h>
#include <math.h>

class differential_odometry {
public:
    // constructor
    differential_odometry();

    void spin();

    void setEncoderTicksPerRotation(int ticks);

    double getEncoderTicksPerRotation();

    void setEncoderTicksPerDegree(int ticks);

    double getEncoderTicksPerDegree();

    void setWheelDiameter(double diameter_);

    double getWheelDiameter();

    void setBaseWidth(double width_);

    double getBaseWidth();

    double turningError(double turn_turn_radius);

    double distanceError(double distance);

    double onPointTurn(double degrees);

    double straightLine(double cm);

    double getEncoder(int i);

    double normalizeAngle(double angle);

    bool has_read_encoders;



private:

    const float distancePerCount = 2.34833 * 1e-04;

    float ticks_per_rotation;
    float ticks_per_degree;
    float diameter, width;
    float enc_left{}, enc_right{};
    float left{}, right{};
    float nowPsi{}, previousPsi{};
    float nowX{}, previousX{};
    float nowY{}, previousY{};
    float dx{}, dr{};

    std::string node_name, prefix;
    int period, publish_tf;

    std::string base_frame, odom_frame;

    // ROS variables
    ros::NodeHandle n;                  //handle for the ROS node
    ros::Subscriber encoders_sub;       //encoders subscriber 
    ros::Publisher odom_pub;            //odometry publisher
    ros::Publisher motor_cmd_pub;       //motor command publisher

    void encoders_callback(const std_msgs::Float32MultiArray::ConstPtr &msg);

    void velocities_callback(const std_msgs::Int16MultiArray::ConstPtr &msg);
};

#endif