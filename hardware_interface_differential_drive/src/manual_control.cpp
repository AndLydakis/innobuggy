#include "manual_control.h"

/**
 * Constructor
 */
manual_control_translation::manual_control_translation() {
    node_name = ros::this_node::getName();

    dr = 0;
    dx = 0;

    //Subscribe to the manual command topic
    twist_sub = n.subscribe("cmd_vel", 10, &manual_control_translation::twist_callback, this);

    //Advertise motion commands for the motor controller
    motion_cmd_pub = n.advertise<std_msgs::Int16MultiArray>("motion_cmd", 10);

    n.getParam(node_name + "/period", period);
    n.getParam(node_name + "/width", width);
    width *= 1e-03;

    ROS_INFO("This is %s in a period:%d", node_name.c_str(), period);
    ROS_INFO("The robot has width:%f", width);
}

/**
 * Callback for twist messages
 * @param msg a geometry_msgs/Twist msg to translate to pwm values
 */
void manual_control_translation::twist_callback(const geometry_msgs::Twist::ConstPtr &msg) {
    //ROS_INFO("Looping when needed");

    dx = msg->linear.x;
    dr = msg->angular.z;

    //ROS_INFO("listening: (%f, %f)", dx, dr);
}

/**
 * Set the messages to a meaningful form
 * @param in command to send to the wheels
 * @return a thresholded
 */
int filter_motor_nonlinearities(int in) {
    if (in > 150)
        return 150;
    else if (in < 60 && in > 0)
        return 60;
    else if (in < -150)
        return -150;
    else if (in > -60 && in < 0)
        return -60;
    else
        return in;
}

/**
 * Main methods, waits for commands in the cmd_vel topic and republishes correct commands
 * to the motion_cmd topic to be handled by the hardware interface
 */
void manual_control_translation::spin() {
    while (ros::ok()) {
        ros::spinOnce();
        usleep(period * 1000);

        std_msgs::Int16MultiArray pwmVelocities;

        pwmVelocities.layout.dim.push_back(std_msgs::MultiArrayDimension());
        pwmVelocities.layout.dim[0].label = "motion_cmd";
        pwmVelocities.layout.dim[0].size = 2;
        pwmVelocities.layout.dim[0].stride = 1;

        pwmVelocities.data.clear();

        float pwm_0 = 1.0 * dx - dr * width / 2;
        float pwm_1 = 1.0 * dx + dr * width / 2;

        // convert to pwm
        // assume that the correllation is 0.5 corresponds to 127
        pwmVelocities.data.push_back(filter_motor_nonlinearities(static_cast<int>((pwm_0 / 0.5) * 100)));
        pwmVelocities.data.push_back(filter_motor_nonlinearities(static_cast<int>((pwm_1 / 0.5) * 100)));

        //ROS_INFO("Publishing velocities to hardware layer [%d, %d]", pwmVelocities.data[0], pwmVelocities.data[1]);

        motion_cmd_pub.publish(pwmVelocities);
    }
}