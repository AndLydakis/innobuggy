#ifndef _MANUAL_CONTROL_TRANSLATION_H_
#define _MANUAL_CONTROL_TRANSLATION_H_

#include <std_msgs/Int16MultiArray.h>
#include <geometry_msgs/Twist.h>
#include <ros/ros.h>

class manual_control_translation {
  public:
    // constructor
    manual_control_translation();
    
    void spin();
    
  private:
    
    float dx, dr, width;
    std::string node_name;
    
    int period;
    
    // ROS variables
    ros::NodeHandle n;                  //handle for the ROS node
    ros::Subscriber twist_sub;          //twist subscriber 
    ros::Publisher motion_cmd_pub;      //motion command publisher
    
    void twist_callback(const geometry_msgs::Twist::ConstPtr&);
};

#endif