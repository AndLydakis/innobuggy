//
// Created by lydakis on 25/06/18.
//
#include "manual_control.h"
int main(int argc, char **argv) {

    ros::init(argc, argv, "translate_twist_node");

    ROS_INFO("Starting the manual control translation node\n");
    manual_control_translation keyboard_control;

    keyboard_control.spin();

    return 0;
}