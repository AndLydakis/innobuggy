//
// Created by lydakis on 25/06/18.
//
#include "hardware_interface.h"

int main(int argc, char** argv) {
    ros::init(argc, argv, "hardware_interface_wheel_stack_node");

    ROS_INFO("Starting the node for the hardware interface\n");
    hardware_interface_differential my_hardware_interface_differential;

    my_hardware_interface_differential.spin();

    return 0;
}