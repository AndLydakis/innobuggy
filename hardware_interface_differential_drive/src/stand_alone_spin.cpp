#include "arduinoSerial.h"

int main() {
 
  Arduino my_arduino("/dev/ttyACM0", 230400);
  int reply;
  float encoders[2];

  while(1) {
    printf("-%ld\n", my_arduino.count++);
    int16_t v[] = {-155, 155};
    my_arduino.send_to_mc(v);

    usleep(20);

    reply = my_arduino.read_from_mc(encoders);
    if (reply) {
      printf("packet received\n");
    } else {
      printf("packet droped\n");
    }
    printf("encoder1:%.1f encoder2:%.1f\n", encoders[0], encoders[1]);
  }
 
  my_arduino.close_serial();
}
