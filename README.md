# innobuggy

This is an attempt to design a ROS package for a differential drive wheeled robot.

### Dependencies
* [phidgets_imu](ros.org/phidgets_imu) and [imu_tools](ros.org/imu_tools) packages to use and visualise imu
* [joy](http://wiki.ros.org/joy) and [teleop_twist_joy](http://wiki.ros.org/teleop_twist_joy) for gamepad tele-operation
* [urg_node](http://wiki.ros.org/urg_node) for the laser scanner
* [laser_scan_matcher](http://wiki.ros.org/laser_scan_matcher) for the laser odometry
* Add the included udev rools to your machine rules
* Requires ForwardX11Trusted to be enabled during ssh for xterm (or ssh with -Y flag)

## Execution
* **To bring up the hardware interface and rviz visualization**:

```bash
inno@inno:~$ roslaunch hardware_interface_differential_drive hardware_interface_and_model.launch
```

This launches the following nodes:

-```ImuFilterNodelet``` & ```imu_manager``` for the phidgets imu

-```hardware_interface``` for the serial communication between ros rPi and arduino

-```robot_state_publisher``` and ```joint_state_publisher``` for setting up the urdf mode for rviz

-```urg_node``` for the Hokuyo scanner

* **To launch the robot_pose_ekf node**:
```bash
inno@inno:~$ roslaunch ekf_launcher robot_pose_ekf.launch
```

* **To have keyboard manual control please execute**:
```bash
inno@inno:~$ roslaunch hardware_interface_differential_drive keyboard_control.launch
```

* **For gamepad control (requires a joy node running, keep button 0 pressed to issue commands)**
```bash
inno@inno:~$ roslaunch harwdare_interface_differential_drive gamepad_control.launch
```

* **For odometry usign the laser scanner**
```bash
inno@inno:~$ roslaunch ekf_launcher laser_scan_matcher.launch 
```

* **For Gazebo Simulation**
```bash
inno@inno:~$ roslaunch simple_robot_description_4wheels innobuggy_gazebo.launch 
```

* **For Testing turning and straight line driving accuracy manually (press button 1 on a gamepad to reset the measurements)**
```bash
inno@inno:~$ rosrun hardware_interface_differential_drive distance_from_encoders.py 
inno@inno:~$ rosrun hardware_interface_differential_drive angle_from_encoders_imu.py 
```

### Version

- Ubuntu Mate 16.04
- ROS Kinetic
- Gazebo 7

### TODOs
- ~~Test Performance IRL~~
- Finish the Gazebo Model
- Test Performance in simulation
- ROS Navigation Integration
- OptiTrack Integration

### Project Members

- [Angelos Plastropoulos](angelos.plastropoulos@innotecuk.com)
- [Andreas Lydakis](andlydakis@gmail.com)
                                       

