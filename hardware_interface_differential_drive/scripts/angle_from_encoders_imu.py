#!/usr/bin/env python

import rospy
import PyKDL
import math

from sensor_msgs.msg import Joy
from sensor_msgs.msg import Imu
from geometry_msgs.msg import Quaternion
from geometry_msgs.msg import PoseWithCovarianceStamped
from std_msgs.msg import Float32MultiArray


def calculate_angle(quat_a, quat_b):
    a = PyKDL.Rotation.Quaternion(quat_a.x, quat_a.y, quat_a.z, quat_a.w)
    b = PyKDL.Rotation.Quaternion(quat_b.x, quat_b.y, quat_b.z, quat_b.w)
    c = a * b.Inverse()
    return c.GetEulerZYX()[0] * 180 / math.pi


def calculate_angle_enc(first_enc, current_enc):
    return (first_enc[0] - current_enc[0]) * (1/13.69)


class CompareAngles:

    def joy_callback(self, joy_msg):
        if joy_msg.buttons[1] == 1:
            self.new_enc = True
            self.new_imu = True
            self.new_ekf = True
            rospy.logerr("Resetting Angles")
            pass

    def encoder_callback(self, encoders_msg):
        if not encoders_msg.data:
            return
        if self.new_enc:
            self.new_enc = False
            self.first_encoders = encoders_msg.data
        self.current_encoders = encoders_msg.data

    def imu_callback(self, imu_msg):
        if self.new_imu:
            self.new_imu = False
            self.first_imu = imu_msg.orientation
        self.current_imu = imu_msg.orientation

    def ekf_callback(self, ekf):
        if self.new_ekf:
            self.new_ekf = False
            self.first_ekf_pose_orientation = ekf.pose.pose.orientation
        self.current_ekf_pose_orientation = ekf.pose.pose.orientation

    def __init__(self):
        self.first_imu = Quaternion()
        self.first_ekf_pose_orientation = Quaternion()
        self.first_encoders = []
        self.current_imu = Quaternion()
        self.current_encoders = []
        self.current_ekf_pose_orientation = Quaternion()

        self.new_imu = True
        self.new_enc = True
        self.new_ekf = True

        rospy.Subscriber("joy", Joy, self.joy_callback, queue_size=10)
        rospy.Subscriber("imu/data", Imu, self.imu_callback, queue_size=10)
        rospy.Subscriber("differential_encoders", Float32MultiArray, self.encoder_callback, queue_size=10)
        rospy.Subscriber("robot_pose_ekf/odom_combined", PoseWithCovarianceStamped, self.ekf_callback, queue_size=10)
        rospy.init_node('compare_angles_node')
        r = rospy.Rate(10)
        while not rospy.is_shutdown():
            if self.current_imu:
                rospy.loginfo("According to the imu the robot has turned " + str(
                    calculate_angle(self.first_imu, self.current_imu)) + " degrees.")

            if self.current_encoders:
                rospy.loginfo(
                    "According to the encoders the robot has turned " + str(
                        calculate_angle_enc(self.first_encoders, self.current_encoders)) + " degrees.")

            if self.current_ekf_pose_orientation:
                rospy.loginfo("According to the EKF the robot has turned " + str(calculate_angle(
                    self.first_ekf_pose_orientation, self.current_ekf_pose_orientation)) + " degrees.")
            r.sleep()


if __name__ == '__main__':
    CompareAngles()
