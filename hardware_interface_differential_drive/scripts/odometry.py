#!/usr/bin/env python

import rospy
import math
from geometry_msgs.msg import Quaternion
from geometry_msgs.msg import Pose2D

from std_msgs.msg import Float32MultiArray
from std_msgs.msg import MultiArrayLayout
from std_msgs.msg import MultiArrayDimension

from nav_msgs.msg import Odometry
from tf.broadcaster import TransformBroadcaster

class DiffTf:
    
    def __init__(self):
        
        rospy.init_node("diff_tf")
        self.nodename = rospy.get_name()
        
        rospy.loginfo("%s started" % self.nodename)
        
        # the rate at which to publish the transform
        self.rate = rospy.get_param('~rate')
        # the name of the base frame of the robot
        self.base_frame_id = rospy.get_param('~base_frame_id') 
        # the name of the odometry reference frame
        self.odom_frame_id = rospy.get_param('~odom_frame_id') 
        # the resolution of the encoder
        self.encoder_resulution = rospy.get_param('~ticks_per_rotation')
        # the width of the robot's base
        self.width = rospy.get_param('~width')
        # the diameter of the wheel
        diameter = rospy.get_param('~diameter')
        # publish TF in case of robot_pose_ekf to avoid 2 parents
        self.publish_tf = rospy.get_param('~publish_tf')
        # the name of the encoders topic prefix
        self.encoders_prefix = rospy.get_param('~board_prefix') 
        
        self.width = self.width*pow(10, -3)
        self.radius = (diameter*pow(10, -3))/2
        
        rospy.loginfo("rate:%.1f, base_link:%s, encoders_topic:%s, odom_link:%s, ticks:%d, width:%.2f, radius:%.2f" \
        % (self.rate, self.base_frame_id, self.encoders_prefix, self.odom_frame_id, self.encoder_resulution, self.width, self.radius))
        
        rospy.Subscriber(self.encoders_prefix + "_encoders", Float32MultiArray, self.encodersCallback)
        
        self.odomPub = rospy.Publisher("odom", Odometry, queue_size=1000)
        
        self.odomBroadcaster = TransformBroadcaster()
        
        self.enc_left = 0
        self.enc_right = 0
        self.left = 0
        self.right = 0
        
        self.nowPsi = 0
        self.previousPsi = 0
        
        self.nowX = 0
        self.previousX = 0
        
        self.nowY = 0
        self.previousY = 0
        
        #self.nowTimestamp = 0
        #self.previousTimestamp = 0
        
        self.dx = 0
        self.dr = 0
    
    def spin(self):
        r = rospy.Rate(self.rate)
        while not rospy.is_shutdown():
            self.update()
            r.sleep()

    
    def update(self):
        #if self.previousTimestamp != self.nowTimestamp:
        
        distancePerCount = 2.34833*pow(10, -4)
                        
        enc_diff_r = (self.enc_right - self.right)
        SR = distancePerCount * enc_diff_r
                        
        enc_diff_l = (self.enc_left - self.left)
        SL = distancePerCount * enc_diff_l
                        
        self.nowPsi = self.previousPsi + (SR-SL)/self.width
        self.nowX = self.previousX + SR*math.cos(self.nowPsi)
        self.nowY = self.previousY + SL*math.sin(self.nowPsi)
                            
        now = rospy.Time.now()
                        
        quaternion = Quaternion()
        quaternion.x = 0.0
        quaternion.y = 0.0
        quaternion.z = math.sin(self.nowPsi/2)
        quaternion.w = math.cos(self.nowPsi/2)
        
        if self.publish_tf:
            self.odomBroadcaster.sendTransform(
                (self.nowX, self.nowY, 0),
                (quaternion.x, quaternion.y, quaternion.z, quaternion.w),
                now,
                self.base_frame_id,
                self.odom_frame_id
                )
                        
        odom = Odometry()

        # header
        odom.header.stamp = now
        odom.header.frame_id = self.odom_frame_id
                        
        # child_frame_id
        odom.child_frame_id = self.base_frame_id
                                    
        # pose
        odom.pose.pose.position.x = self.nowX
        odom.pose.pose.position.y = self.nowY
        odom.pose.pose.position.z = 0
        odom.pose.pose.orientation = quaternion
        odom.pose.covariance[0] = 0.01
        odom.pose.covariance[7] = 0.01;
        odom.pose.covariance[14] = 0.01;
        odom.pose.covariance[21] = 0.1;
        odom.pose.covariance[28] = 0.1;
        odom.pose.covariance[35] = 0.1;

        # twist
        odom.twist.twist.linear.x = self.dx
        odom.twist.twist.linear.y = 0
        odom.twist.twist.angular.z = self.dr
        odom.twist.covariance = odom.pose.covariance;

        #rospy.loginfo("Odom: %s", odom)		
        self.odomPub.publish(odom)
        
        # assignments to previuos place-holders
        self.previousPsi = self.nowPsi
        self.previousX = self.nowX
        self.previousY = self.nowY
        self.left = self.enc_left
        self.right = self.enc_right
    
    def encodersCallback(self,  msg):
         
        self.enc_left = msg.data[0] 
        self.enc_right = msg.data[1]
        
        #rospy.loginfo("received: left %.0f right %.0f " % (self.enc_left, self.enc_right))

if __name__ == '__main__':
    diffTf = DiffTf()
    diffTf.spin()
