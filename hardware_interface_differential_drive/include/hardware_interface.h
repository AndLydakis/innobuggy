#include <ros/ros.h>
#include <std_msgs/Int16MultiArray.h>
#include <std_msgs/Int16.h>
#include <string.h>
#include <stdint.h>
#include <vector>

#include "arduinoSerial.h"
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/Int16MultiArray.h>

class hardware_interface_differential {
public:
    // constructor & destructor
    hardware_interface_differential();

    ~hardware_interface_differential();

    // spin the comms
    void spin();


private:
    // arduino serial comm object
    Arduino *my_arduino;

    // arduino comm details
    std::string node_name;
    std::string port;

    int baud_rate, period;

    // how to prefix the topic which we publish the encoders
    std::string prefix;

    // velocities
    int16_t vel1 = 0, vel2 = 0;

    // ROS variables
    ros::NodeHandle n;        //handle for the ROS node
    ros::Subscriber vel_sub;  //velocities subscriber [listens motion_cmd and select]
    ros::Publisher enc_pub;   //encoders publisher

    void velocities_callback(const std_msgs::Int16MultiArray::ConstPtr &msg);

};
