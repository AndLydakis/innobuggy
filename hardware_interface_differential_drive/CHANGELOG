0.0.27 hardware_interface_differential_drive
Change from custom IMU support package to official Phidget's drivers package.

0.0.26 hardware_interface_differential_drive
In this update, we included the following changes:
- update to udev.rules file to work with innobuggy
- updates to launch files
- imu added to start automayically with the hardware interface

0.0.25 hardware_interface_differential_drive
In this update we introduced the following changes:
- odometry node re-written in C++
- config files re-arranged and tidied up
- launch files tidied up
- housekeeping in general

0.0.24 hardware_interface_differential_drive
This is a major update containing the following changes:
- Arduino sketch updated to include time interrupts
- Encoders messages changed to simple Float32MultiArrays
- Differential odometry script added/updated
- Constant velocity motion added/updated

0.0.23 hardware_interface_wheel_stack
In this version we changed the arduino sketches templates to perform communication
in a time interrupt (M0 Pro compatible)
In ROS part only the stand alone spin node has been modified and tested.

0.0.17 hardware_interface_base
In this version we changed the arduino sketches templates to perform communication
in a time interrupt (Due compatible)
In ROS part only the stand alone spin node has been modified and tested.

0.0.16 hardware_interface_base && 0.0.22 hardware_interface_wheel_stack
Config files updated to be the same with the actual robot and height margin minimised

0.0.15 hardware_interface_base
serialFlush removed from the arduinoSerial script because it was causing unexpected
problems.

0.0.21 hardware_interface_wheel_stack
serialFlush removed from the arduinoSerial script because it was causing unexpected
problems.

0.0.20 hardware_interface_wheel_stack
In this version we introduced the following things:
- another input to the hardware_interface nodes to listen for a specific yaw command
- test script for yaw commands

0.0.14 hardware_interface_base
In this version we changed the following things:
- convenient scaling of the communication period
- added serial flush to the ROS side

0.0.19 hardware_interface_wheel_stack
In this version we changed the following things:
- convenient scaling of the communication period
- added serial flush to the ROS side
- housekeeping

0.0.18 hardware_interface_wheel_stack
Sample file for using persistent usb serial devices added to doc folder.
Config file changed accordingly.

0.0.13 hardware_interface_base
Minor fix to the spinning rate of the hardware interface.

0.0.17 hardware_interface_wheel_stack
In this version we fix a major bug that was the change from sending PWMs instead of the range
(-128, 128) to (-255, 255) whoch is the correct one.

0.0.12 hardware_interface_base
Typo fixed

0.0.16 hardware_interface_wheel_stack
The test_motion_cmd python script updated to publish 8 values instead of 2.

0.0.15 hardware_interface_wheel_stack
Arduino sketch bug fixed.

0.0.11 hardware_interface_base
Minor bug fix

0.0.14 hardware_interface_wheel_stack
Arduino sketch updated with a different delay technique to allow absolute encoders work in the loop.

0.0.10 hardware_interface_base
Minor changes in this version that we did after the first attemp to connect the lcd shield.
- connection parameters updated to slow down the communication
- debug messages added to the ROS hardware interface to help with the debugging

0.0.9 hardware_interface_base
In this version we have introduced the following changes:
- Height.srv changed to uint8
- The height_server changed to support uint8 instead of float32

0.0.8 hardware_interface_base
The response of the heihg service changed for boolean success/failure to the final height achieved.

0.0.7 hardware_interface_base
One more launch file added to bring up both the height server and the hardware interface.
Height server returned to the first approach of multithreading.

0.0.13 hardware_interface_wheel_stack
The CMakeLists.txt updated to include the dependeny from the Encoders message during the compilation.

0.0.12 hardware_interface_wheel_stack
In this version we have introduced the following changes:
- arduino_serial throws exceptions when cannot find in the specified port an arduino
connected
- change the arduino_parameters.yaml to host for arduino M0 pro boards
- create a new launch file to fire all the wheel stack controllers

0.0.6 hardware_interface_base
The due arduino sketch changed from driller command to requested height.
Old scripts regarding height mechanism based purely on topics removed.

0.0.5 hardware_interface_base
In this version we have introduced the following changes:
- height_server added to accept height requests from JSON server, forward them in
the hardware_interface (platform) and then check when the transition is completed
and send back the ackownledgement.
- hardware_interface changed to support the communication with the height_server
- arduino_serial throws exceptions when cannot find in the specified port an arduino
connected

0.0.4 hardware_interface_base
General feedback changed to height

0.0.3 hardware_interface_base
One more ultrasonic sensor added the data tupple which is returned from arduino.
(at this point only the arduino due sketch updated)
+ hardware_interface_scissor_lifter removed because we changed the architecture

hardware_interface_arduino
readme file updated to document briefly the packages provided.

0.0.2 hardware_interface_base
Few minor changes and experiments of how it works with Mega board.

0.0.1 hardware_interface_base
This is the first cut of the base inerface. Stand alone version in an Arduino Due seems that
works fine.

0.0.4 hardware_interface_scissor_lifter
Stand alone and ROS working version for scissor lifter board tested with arduino Uno.
The baudrate set to 115200 and the interval to 10ms and seems that work without problems.

0.0.3 hardware_interface_scissor_lifter
ROS working version for scissor lifter board tested with arduino MO and M0 pro.

0.0.2 hardware_interface_scissor_lifter
This is the first working version. It uses the stand alone script to spin the communications.
Tested so far with arduino M0 pro.

0.0.1 hardware_interface_scissor_lifter
First cut of the scissor lifter interface not working at this point.

0.0.11 hardware_interface_wheel_stack
In this version, we refactored the code to accommodate the 3 individual boards
- boards for wheel stacks
- board for scissor lifter
- board for base

0.0.10 hardware_interface_arm
In this version, we added a second sketch which operates as template without pin declerations.
We added the feature to read from a common velocity vector specific elements whitch corresponds
to specific wheel (front/rear - left/right). Finally, we added a doc folder for documentation.

0.0.9 hardware_interface_arm
Houskeeping

0.0.8 hardware_interface_arm
Arduino sketch updated for the real test in inno-buggy platform.
Few adjastments and modifications to support real hardware data flow.

0.0.7 hardware_interface_arm
A destructor added to close the serial connection

0.0.6 hardware_interface_arm
Arduino sketch updated to include constants in a header file

0.0.5 hardware_interface_arm
We added a new folder containing the arduino sketches with need to be uploaded to
arduino

0.0.4 hardware_interface_arm
In this version, we have introduced the following changes:
- change from ROS rate to simple sleep delay (for some reason with rate we had some drops)
- comments to hardware_interface header file
- created a launch file for test velocities
- test velocities reads rate from config
- spin renamed to stand_alone_spin test

0.0.3 hardware_interface_arm
In this version we split the interface from implementation regarding hardware_interface,
we read ROS parameters from the server and load from config files. Some CMakeLists.txt
diet.

0.0.2 hardware_interface_arm
First cut of the hardware_interface node added. Just listens to a demo topic.

0.0.1 hardware_interface_arm
Initial Commit
