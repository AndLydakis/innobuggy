#include "config.h"
#include "avdweb_SAMDtimer.h"

// struct of the outgoing package inlcuding encoders
typedef struct {
  char startMark;
  float encoder1;
  float encoder2;
  byte checksum;
  char endMark;
} __attribute__((__packed__))data_packet_t; 

data_packet_t wheels;

const int baud_rate = SERIAL_BAUD_RATE;
const byte numBytes = NUM_OF_INCOMING_BYTES;
const byte numPayloadBytes = numBytes - 2;
byte receivedBytes[numBytes];
byte polarity;
byte v1, v2;
int v1_sign, v2_sign;

boolean newData = false;

volatile float encoder1=0;
volatile float encoder2=0;

void timer4_interrupt(struct tc_module *const module_inst) 
{ 
  /*
  unsigned long StartTime = millis();
  Serial.print(StartTime);
  Serial.println(" hey");
  */
  // ---- Send encoders to rPi (ROS hardware interface)
  sendEncoders();
   
  // ---- Receive the motor velocities from rPi (ROS hardware interface) 
  receiveMotorVelocities();
    
  // ---- Log input values
  newDataAvailable();
}

SAMDtimer timer4 = SAMDtimer(4, timer4_interrupt, 20000);

void setup()                    
{
  // begin serial in programming USB serial for debugging
  Serial.begin(baud_rate);
  // begin serial in native USB for connection with rPi3
  SerialUSB.begin(baud_rate);           
  
  // initial values for encoders (floats)
  wheels.startMark = '<';
  wheels.encoder1 = 0.0;
  wheels.encoder2 = 0.0;
  wheels.endMark = '>';

  timer4.attachInterrupt(timer4_interrupt);
}

void loop()                       
{
  /*
  // ---- Send encoders to rPi (ROS hardware interface)
  sendEncoders();
   
  delay(INTERVAL);

  // ---- Receive the motor velocities from rPi (ROS hardware interface) 
  receiveMotorVelocities();
    
  // ---- Log input values
  newDataAvailable();
  */
}

//-----------------------------------------------------------
//

bool calc_incoming_checksum() 
{
  byte check_sum;
  int sum = 0;
  for (int i = 0; i < numPayloadBytes-1; i++) {
    
    sum += receivedBytes[i];
  }

  check_sum = 255 - (sum%255);
 
  SerialUSB.println(check_sum, HEX);

  if (check_sum == receivedBytes[numPayloadBytes - 1]) {
    return true;
  } else {
    return false;
  }
}


//-----------------------------------------------------------
//

byte calc_outgoing_checksum() 
{ 
  unsigned long uBufSize = sizeof(data_packet_t);
  byte pBuffer[uBufSize];
  int sum = 0;
  byte check_sum;
  
  //SerialUSB.print("--> ");
  //SerialUSB.println(uBufSize);
  
  memcpy(pBuffer, &wheels, uBufSize);
  for(int i = 1; i<uBufSize-2;i ++) {
    //SerialUSB.println(pBuffer[i], HEX);
    sum += pBuffer[i];
  }
  //SerialUSB.println(sum, HEX);

  check_sum = 255 - sum%255;
  //SerialUSB.print("--> ");
  //SerialUSB.println(check_sum, HEX);

  return check_sum;
}


//-----------------------------------------------------------
//

void sendEncoders()
{
 
  wheels.startMark = '<';
  wheels.encoder1 = encoder1;
  wheels.encoder2 = encoder2;
  wheels.endMark = '>';

  logOutput();
  
  // calculate the check_sum of the outgoing frame and send it 
  wheels.checksum = calc_outgoing_checksum();
  
  unsigned long uBufSize = sizeof(data_packet_t);
  char pBuffer[uBufSize];
  
  memcpy(pBuffer, &wheels, uBufSize);
  for(int i = 0; i<uBufSize;i ++) {
   SerialUSB.print(pBuffer[i]);
  }
  SerialUSB.flush();
  
}


//-----------------------------------------------------------
//

void receiveMotorVelocities() 
{
  static boolean recvInProgress = false;
  static byte ndx = 0;
  byte startMarker = 0x3C;
  byte endMarker = 0x3E;
  byte rc;

  //SerialUSB.println("Debug: Intro");

  while (SerialUSB.available() > 0 && newData == false) {
    rc = SerialUSB.read();

    //SerialUSB.println("Debug: Receive bytes");
    
    if (recvInProgress == true) {

      //SerialUSB.println("Debug: Receive in progress");
      
      if (rc != endMarker) {

        //SerialUSB.println("Debug: Fill-in the array");
        
        receivedBytes[ndx] = rc;
        ndx++;
        if (ndx >= numBytes) {
          ndx = numBytes - 1;
        } 
      } else {
        //SerialUSB.println("Debug: Filled array");
        recvInProgress = false;
        ndx = 0;
        newData = true;
      }
    } else if (rc == startMarker) {
      recvInProgress = true;
    } 
  }
}


//-----------------------------------------------------------
//

// This is where the data are available, because it passes the checksum check!
void newDataAvailable() {
  if (newData == true) {
    if (calc_incoming_checksum()) {
      
      //SerialUSB.println("Debug: Valid data received");

      //SerialUSB.println("Debug: Received ");

      polarity = receivedBytes[0];
      //Serial.println(receivedBytes[0], BIN);
      
      v1_sign = getPolarity(polarity, 1);
      v1 = receivedBytes[1];
      //SerialUSB.println(v1_sign);
    
      v2_sign = getPolarity(polarity, 2);
      v2 = receivedBytes[2];
      //SerialUSB.println(v2_sign);
    
      Serial.print("v1 = ");
      Serial.print(v1_sign*(int)v1);
      Serial.print(" v2 = ");
      Serial.println(v2_sign*(int)v2);    
    } else {
      Serial.println("Debug: NOT valid data received");
    }

    // unlock receiving procedure
    newData = false;
  }
}


//-----------------------------------------------------------
//

void logOutput()
{ 
  Serial.print("Outgoing data: ");
  Serial.print(encoder1);
  Serial.print(" / ");
  Serial.print(encoder2);
  Serial.print(" - ");
  Serial.println(wheels.checksum, HEX);
}


//-----------------------------------------------------------
//

int getPolarity(byte input, int index)
{
  if(bitRead(input, index-1)) {
    return -1; 
  } else {
    return 1;
  }
}
