#include "arduinoSerial.h"

Arduino::Arduino(const char *device, const int baud) {
  count = 0;

  if ((fd = serialOpen (device, baud)) < 0)
  {
    fprintf (stderr, "[ArduinoSerial]: Unable to open serial device: %s\n", strerror (errno)) ;
		throw -1;
  } else {
    fprintf (stderr, "[ArduinoSerial]: Open serial device on %s at %d\n", device, baud) ;
  }
}

/**
 * Read from motor controllers
 * @param encoders a pair of floats to write the encoder values in
 * @return 0 if something fails, 1 otherwise
 */
int Arduino::read_from_mc(float *encoders)
{
  int accepted = 0;
  // read
  //printf("*%d\n", serialDataAvail (fd));
  while(serialDataAvail (fd) >= 10) {
    int c = serialGetchar(fd);
    if (c == 60) {
      read(fd, inputBuffer, 10);
      u.b[3] = inputBuffer[3];
      u.b[2] = inputBuffer[2];
      u.b[1] = inputBuffer[1];
      u.b[0] = inputBuffer[0];
      encoders[0] = u.f;
      //printf("%f\n", u.f);

      v.b[3] = inputBuffer[7];
      v.b[2] = inputBuffer[6];
      v.b[1] = inputBuffer[5];
      v.b[0] = inputBuffer[4];
      encoders[1] = v.f;
      //printf("%f\n", v.f);

      //printf("%x\n", buffer[8]);

      //printf("%c\n", buffer[9]);

      //accepted = calc_incoming_checksum(inputBuffer);
	  //printf("*%f %f\n", u.f, v.f);

	  unsigned char check_sum;
      int sum = 0;
      int numPayloadBytes = 8;

      for (int i = 0; i < numPayloadBytes; i++) {
        sum += inputBuffer[i];
      }

      check_sum = 255 - (sum%255);

      //printf("cks1: %x\n", check_sum);
      //printf("cks2: %x\n", inputBuffer[numPayloadBytes]);

      if (check_sum == inputBuffer[numPayloadBytes]) {
    	accepted = 1;
  	  } else {
    	accepted = 0;
  	  }
      /*if (accepted) {
        printf("packet received\n");
      } else {
        printf("packet droped\n");
      }*/
      /*
      for(int i=0; i<8; i++) {
      printf("-->%x\n", buffer[i]);
    }*/
	//printf("reception finished\n");
    fflush (stdout) ;
    }

  }
  return accepted;
}

/**
 * Send velocities to motor controllers
 * @param velocities two velocities as int for the left and right
 * motor controllers correspondingly
 */
void Arduino::send_to_mc(int16_t *velocities)
{
  //startMarker
  outputBuffer[0] = '<';

  //velocities polarities initialisation
  outputBuffer[1] = 0;

  //velocities
  outputBuffer[2] = (unsigned char) abs(velocities[0]);
  outputBuffer[3] = (unsigned char) abs(velocities[1]);

	//printf("++>%x, %x\n", velocities[0], velocities[1]);
	//printf("-->%x, %x\n", outputBuffer[2], outputBuffer[3]);

  // outgoing checksum initialisation
  outputBuffer[4] = 0;

  //endMarker
  outputBuffer[5] = '>';

  config_velocityPolarity(outputBuffer, velocities);
  outputBuffer[4] = calc_outgoing_checksum(outputBuffer);
  //printf("-->%x\n", outputBuffer[4]);

  write(fd, outputBuffer, 6);
}

void Arduino::config_velocityPolarity(unsigned char *receivedBytes, int16_t *velocities)
{
  unsigned char v_polarity = receivedBytes[1];

	int16_t vel1 = velocities[0];
  int16_t vel2 = velocities[1];

  //int8_t iVel1 = (int8_t)vel1;
  //int8_t iVel2 = (int8_t)vel2;

  //printf("*%x\n", v_porarity);
  //printf("*%d\n", my);
  //printf("*%x\n", vel2);

  if (vel1 < 0) {
    v_polarity |= 1UL << 0;
    //receivedBytes[2] = (unsigned char)(-iVel1);
  }
  if (vel2 < 0) {
    //receivedBytes[3] = (unsigned char)(-iVel2);
    v_polarity |= 1UL << 1;
  }
  receivedBytes[1] = v_polarity;
  //printf("*%x\n", v_polarity);
}

int Arduino::calc_incoming_checksum(unsigned char *receivedBytes)
{
  unsigned char check_sum;
  int sum = 0;
  int numPayloadBytes = 8;

  for (int i = 0; i < numPayloadBytes; i++) {
    sum += receivedBytes[i];
  }

  check_sum = 255 - (sum%255);

  printf("cks1: %x\n", check_sum);
  printf("cks2: %x\n", receivedBytes[numPayloadBytes]);

  if (check_sum == receivedBytes[numPayloadBytes]) {
    return 1;
  } else {
    return 0;
  }
}

unsigned char Arduino::calc_outgoing_checksum(unsigned char *receivedBytes)
{
  int sum = 0;
  unsigned char check_sum;

  for(int i = 1; i<6-2;i ++) {
    //printf("*%x\n", receivedBytes[i]);
    sum += receivedBytes[i];
  }

  check_sum = 255 - sum%255;

  return check_sum;
}

/**
 * Closes the serial connections
 */
void Arduino::close_serial ()
{
  close (fd) ;
}
