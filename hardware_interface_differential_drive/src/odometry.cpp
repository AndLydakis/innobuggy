#include "odometry.h"

# define M_PI           3.14159265358979323846

/**
 * Constructor
 */
differential_odometry::differential_odometry() {
    node_name = ros::this_node::getName();

//    n.getParam(node_name + "/prefix", prefix);
//    encoders_sub = n.subscribe(prefix + "_encoders", 10, &differential_odometry::encoders_callback, this);
    n.getParam("differential/prefix", prefix);
    encoders_sub = n.subscribe("differential_encoders", 10, &differential_odometry::encoders_callback, this);

    odom_pub = n.advertise<nav_msgs::Odometry>("odom", 10);

    n.getParam(node_name + "/period", period);
    n.getParam(node_name + "/width", width);
    width *= 1e-03;
    n.getParam(node_name + "/diameter", diameter);
    diameter *= 1e-03;
    n.getParam(node_name + "/base_frame_id", base_frame);
    n.getParam(node_name + "/odom_frame_id", odom_frame);
    n.getParam(node_name + "/ticks_per_rotation", ticks_per_rotation);
    n.getParam(node_name + "/ticks_per_degree", ticks_per_degree);

    n.getParam(node_name + "/publish_tf", publish_tf);

    ROS_INFO("This is the %s node spinning with a period:%d", node_name.c_str(), period);
    ROS_INFO("The robot has width:%f and diameter:%f", width, diameter);
    ROS_INFO("The encoders require %f ticks per rotation", ticks_per_rotation);
    ROS_INFO("The encoders require %f ticks per degree", ticks_per_degree);
    ROS_INFO("base_frame:%s and odom_frame:%s", base_frame.c_str(), odom_frame.c_str());
    ROS_INFO("publish_tf:%d", publish_tf);

    has_read_encoders = false;
}

/**
 * Encoders callback
 * @param msg a 2 float message of the form [left_encoder_value, right_encoder_value]
 */
void differential_odometry::encoders_callback(const std_msgs::Float32MultiArray::ConstPtr &msg) {

    enc_left = msg->data[0];
    enc_right = msg->data[1];
    has_read_encoders = true;
//    ROS_INFO("listening encoders: (%f, %f)", enc_left, enc_right);
}

/**
 * The main method, constantly spins querying the encoders and
 * sending commands if messages have arrived in the corresponding topic
 */
void differential_odometry::spin() {

    float enc_diff_r{}, SR{};
    float enc_diff_l{}, SL{};
    static tf::TransformBroadcaster br;

    while (ros::ok()) {

        ros::spinOnce();
        usleep(period * 1000);

        enc_diff_r = (enc_right - right);
        SR = distancePerCount * enc_diff_r;

        enc_diff_l = (enc_left - left);
        SL = distancePerCount * enc_diff_l;

        nowPsi = previousPsi + (SR - SL) / width;
        nowX = previousX + SR * cos(nowPsi);
        nowY = previousY + SL * sin(nowPsi);

        ros::Time now = ros::Time::now();

        tf::Quaternion q(0.0, 0.0, sin(nowPsi / 2), cos(nowPsi / 2));

        if (publish_tf) {
            tf::Transform transform;
            transform.setOrigin(tf::Vector3(nowX, nowY, 0.0));
            transform.setRotation(q);

            br.sendTransform(tf::StampedTransform(transform, now, odom_frame, base_frame));
        }

        geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(nowPsi);
        nav_msgs::Odometry odom_msg;

        // header
        odom_msg.header.stamp = now;
        odom_msg.header.frame_id = odom_frame;
        odom_msg.child_frame_id = base_frame;

        // pose
        odom_msg.pose.pose.position.x = nowX;
        odom_msg.pose.pose.position.y = nowY;
        odom_msg.pose.pose.position.z = 0;

        odom_msg.pose.pose.orientation = odom_quat;

        odom_msg.pose.covariance[0] = 0.01;
        odom_msg.pose.covariance[7] = 0.01;
        odom_msg.pose.covariance[14] = 1000000.0;
        odom_msg.pose.covariance[21] = 1000000.0;
        odom_msg.pose.covariance[28] = 1000000.0;
        odom_msg.pose.covariance[35] = 0.1;

        // twist
        odom_msg.twist.twist.linear.x = dx;
        odom_msg.twist.twist.linear.y = 0;
        odom_msg.twist.twist.angular.z = dr;
        odom_msg.twist.covariance = odom_msg.pose.covariance;

        odom_pub.publish(odom_msg);

        // assignments to previous place-holders
        previousPsi = nowPsi;
        previousX = nowX;
        previousY = nowY;
        left = enc_left;
        right = enc_right;

    }
}

double differential_odometry::turningError(double turn_radius) {

    return 0;
}

double differential_odometry::distanceError(double distance) {

    return 0;
}

/**
 * Rotates the robot in place
 * @param degrees how many degrees to rotate (positive for counter clockwise, negative for clockwise)
 * @return 0
 */
double differential_odometry::onPointTurn(double degrees = 90) {
    n.getParam(ros::this_node::getName() + "/degrees", degrees);
    //calculate necessary encoder ticks for turn
    degrees = normalizeAngle(degrees);
    int sign = (degrees >= 0) ? 1 : -1;
    int goalTicks = static_cast<int>(ticks_per_degree * abs(degrees));
    int curEncRight = getEncoder(1);
    int curEncLeft = getEncoder(0);

    int goalTicksRight = curEncRight + (sign * goalTicks);
    int goalTicksLeft = curEncLeft - (sign * goalTicks);
    ROS_INFO("#----------------------------------------#");
    ROS_INFO("Rotating in place %f degrees", degrees);
    ROS_INFO("Goal encoder ticks: %d", goalTicks);
    ROS_INFO("Right encoder value: %d/%d", curEncRight, goalTicksRight);
    ROS_INFO("Left encoder value: %d/%d", curEncLeft, goalTicksLeft);
    ROS_INFO("#----------------------------------------#");

    auto conditionRight = ((sign >= 0) ? std::function<bool()>(
            [this, goalTicksRight]() mutable { return enc_right < goalTicksRight; })
                                       : std::function<bool()>(
                    [this, goalTicksRight]() mutable { return enc_right > goalTicksRight; }));

    auto conditionLeft = ((sign >= 0) ? std::function<bool()>(
            [this, goalTicksLeft]() mutable { return enc_left > goalTicksLeft; })
                                      : std::function<bool()>(
                    [this, goalTicksLeft]() mutable { return enc_left < goalTicksLeft; }));

    motor_cmd_pub = n.advertise<std_msgs::Int16MultiArray>("motion_cmd", 10);
    while (conditionRight() || conditionLeft()) {
        std_msgs::Int16MultiArray pwmVelocities;
        pwmVelocities.layout.dim.push_back(std_msgs::MultiArrayDimension());
        pwmVelocities.layout.dim[0].label = "motion_cmd";
        pwmVelocities.layout.dim[0].size = 2;
        pwmVelocities.layout.dim[0].stride = 1;

        pwmVelocities.data.clear();
        int packet = (70 / 0.5) * 100;

//        ROS_INFO("Right encoder value: %f/%d", enc_right, goalTicksRight);
//        ROS_INFO("Left encoder value: %f/%d", enc_left, goalTicksLeft);
//        ROS_INFO("Sending %d to left encoder, %d to right encoder",
//                 (enc_left > (sign * goalTicksLeft)) ? -sign * packet : 0,
//                 (enc_right < (sign * goalTicksRight)) ? sign * packet : 0);
//        ROS_INFO("#----------------------------------------#");
        // convert to pwm
        // assume that the correlation is 0.5 corresponds to 127
        (conditionLeft()) ? pwmVelocities.data.push_back(-sign * packet)
                          : pwmVelocities.data.push_back(
                0);
        (conditionRight()) ? pwmVelocities.data.push_back(sign * packet)
                           : pwmVelocities.data.push_back(0);
        motor_cmd_pub.publish(pwmVelocities);
        ros::spinOnce();
    }
    ROS_INFO("Final right encoder ticks: %f", getEncoder(1));
    ROS_INFO("Final left encoder ticks: %f", getEncoder(0));

    std_msgs::Int16MultiArray pwmVelocities;
    pwmVelocities.layout.dim.push_back(std_msgs::MultiArrayDimension());
    pwmVelocities.layout.dim[0].label = "motion_cmd";
    pwmVelocities.layout.dim[0].size = 2;
    pwmVelocities.layout.dim[0].stride = 1;
    pwmVelocities.data.push_back(0);
    pwmVelocities.data.push_back(0);
    motor_cmd_pub.publish(pwmVelocities);
    ros::spinOnce();

    ROS_INFO("Exiting");
    return 0;
}

/**
 * Move the robot in a straight line
 * @param cm Centimeters to move (positive for forward, negative for backwards)
 * @return 0
 */
double differential_odometry::straightLine(double cm = 10) {

    n.getParam(ros::this_node::getName() + "/cm", cm);
    //calculate necessary encoder ticks for straight line
    int goalTicks = (360 / (M_PI * diameter * 1000)) * cm * 100;
    //set movement direction
    int sign = (cm > 0) ? 1 : -1;
    bool first_measurement = false;
    int current_val = getEncoder(0);

    goalTicks += current_val;
    ROS_INFO("#----------------------------------------#");
    ROS_INFO("Moving Straight for %f cm", cm);
    ROS_INFO("Starting left encoder value: %d", current_val);
    ROS_INFO("Goal encoder ticks: %d", goalTicks);
    ROS_INFO("#----------------------------------------#");
    motor_cmd_pub = n.advertise<std_msgs::Int16MultiArray>("motion_cmd", 10);
    while (abs(enc_left) < goalTicks) {
        //ROS_INFO("%d / %d ticks", abs(enc_left - current_val), goalTicks);
        //current_val = enc_left;
        std_msgs::Int16MultiArray pwmVelocities;
        pwmVelocities.layout.dim.push_back(std_msgs::MultiArrayDimension());
        pwmVelocities.layout.dim[0].label = "motion_cmd";
        pwmVelocities.layout.dim[0].size = 2;
        pwmVelocities.layout.dim[0].stride = 1;

        pwmVelocities.data.clear();
        int packet = (sign * 70 / 0.5) * 100;
        //ROS_INFO("Sending %d to encoders", packet);
        // convert to pwm
        // assume that the correlation is 0.5 corresponds to 127
        pwmVelocities.data.push_back(packet);
        pwmVelocities.data.push_back(packet);
        motor_cmd_pub.publish(pwmVelocities);
        ros::spinOnce();
    }
    ROS_INFO("Final left encoder ticks: %d", getEncoder(0));

    std_msgs::Int16MultiArray pwmVelocities;
    pwmVelocities.layout.dim.push_back(std_msgs::MultiArrayDimension());
    pwmVelocities.layout.dim[0].label = "motion_cmd";
    pwmVelocities.layout.dim[0].size = 2;
    pwmVelocities.layout.dim[0].stride = 1;
    pwmVelocities.data.push_back(0);
    pwmVelocities.data.push_back(0);
    motor_cmd_pub.publish(pwmVelocities);
    ros::spinOnce();

    ROS_INFO("Exiting");
    return 0;
}

/**
 * Sets the ticks required for a full wheel rotation
 * @param ticks the ticks required for a full wheel rotation
 */
void differential_odometry::setEncoderTicksPerRotation(int ticks) {
    ticks_per_rotation = ticks;
}

/**
 * Returns the ticks required for a full rotation
 * @return the ticks required for a full wheel rotation
 */
double differential_odometry::getEncoderTicksPerRotation() {
    return ticks_per_rotation;
}

/**
 * Sets the encoder ticks required for a one degree in place turn
 * @param ticks the encoder ticks required for a one degree in place turn
 */
void differential_odometry::setEncoderTicksPerDegree(int ticks) {
    ticks_per_degree = ticks;
}


/**
 * Returns the encoder ticks required for a one degree in place turn
 * @return ticks the encoder ticks required for a one degree in place turn
 */
double differential_odometry::getEncoderTicksPerDegree() {
    return ticks_per_degree;
}

/**
 * Set the wheel diameter in mm
 * @param diameter_ the wheel diameter in mm
 */
void differential_odometry::setWheelDiameter(double diameter_) {
    diameter = diameter_;
}

/**
 * Get the wheel diameter in mm
 * @return the wheel diameter in mm
 */
double differential_odometry::getWheelDiameter() {
    return diameter;
}

/**
 * Set the robot base width in cm
 * @param width_ the robot base width in cm
 */
void differential_odometry::setBaseWidth(double width_) {
    width = width_;
}

/**
 * Return the robot base width in cm
 * @return the robot base width in cm
 */
double differential_odometry::getBaseWidth() {
    return width;
}

/**
 * Get the current encoder values
 * @param enc the encoder to query, 1 for right, 0 for left
 * @return
 */
double differential_odometry::getEncoder(int enc) {
    if (enc == 0)return enc_left;
    return enc_right;
}

/**
 * Normalize an angle between -179 and 180 degrees
 * @param angle an angle in degrees
 * @return the normalized angle
 */
double differential_odometry::normalizeAngle(double angle) {
    angle = fmod(angle + 180, 360);
    if (angle < 0)
        angle += 360;
    return angle - 180;
}