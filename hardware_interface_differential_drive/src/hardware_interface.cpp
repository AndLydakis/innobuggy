#include "hardware_interface.h"

/**
 * Constructor
 */
hardware_interface_differential::hardware_interface_differential() {
    node_name = ros::this_node::getName();

    vel_sub = n.subscribe("motion_cmd", 10, &hardware_interface_differential::velocities_callback, this);

    n.getParam(node_name + "/prefix", prefix);

    enc_pub = n.advertise<std_msgs::Float32MultiArray>(prefix + "_encoders", 10);

    n.getParam(node_name + "/port", port);
    n.getParam(node_name + "/baud", baud_rate);
    n.getParam(node_name + "/period", period);

    try {
        my_arduino = new Arduino(port.c_str(), baud_rate);
    } catch (int e) {
        ROS_FATAL("Problem bringing up Arduino");
        ros::shutdown();
        exit(EXIT_FAILURE);
    }

    ROS_INFO("At port:%s with baud:%d and transmission period T:%d", port.c_str(), baud_rate, period);
}

/**
 * Destructor (shuts down the connection to the arduino)
 */
hardware_interface_differential::~hardware_interface_differential() {
    fprintf(stderr, "Shutting down the serial connection\n");
    my_arduino->close_serial();
}

/**
 * Callback that handles velocities messages target to the wheels
 * @param msg a message of the form [left_wheel_vel, right_wheel_vel]
 */
void hardware_interface_differential::velocities_callback(const std_msgs::Int16MultiArray::ConstPtr &msg) {

//    ROS_INFO("vel1: [%d] - vel2: [%d]", msg->data[0], msg->data[1]);

    vel1 = msg->data[0];
    vel2 = msg->data[1];
}

/**
 * Main method of hardware_interface_differential
 * Continuously sends velocities to the motor controllers
 * polls the encoders and republishes the values in /differential_encoders
 */
void hardware_interface_differential::spin() {
    int reply = 0;
    float encoders[2];

    while (ros::ok()) {
//        ROS_INFO("Looping");
//        printf("packet #%ld\n", my_arduino->count++);
//        printf(" - \n");

//        Send velocities to motor controller
//        ROS_INFO("Sending to mc vel1: [%d] - vel2: [%d]", vel1, vel2);
        int16_t v[] = {vel1, vel2};
        my_arduino->send_to_mc(v);

        ros::spinOnce();
        usleep(period * 1000);


        //Read ticks from encoders
        reply = my_arduino->read_from_mc(encoders);
//        ROS_INFO("Reply %d", reply);
        if (reply) {
            std_msgs::Float32MultiArray encodersTicks;

            encodersTicks.layout.dim.push_back(std_msgs::MultiArrayDimension());
            encodersTicks.layout.dim[0].label = "ultrasonics";
            encodersTicks.layout.dim[0].size = 2;
            encodersTicks.layout.dim[0].stride = 1;

            encodersTicks.data.clear();
            encodersTicks.data.push_back(encoders[0]);
            encodersTicks.data.push_back(encoders[1]);

            enc_pub.publish(encodersTicks);
//            ROS_INFO("Packet received: encoder1:%.1f encoder2:%.1f", encoders[0], encoders[1]);
        } else {
            ROS_ERROR("Packet dropped: encoder1:%.1f encoder2:%.1f", encoders[0], encoders[1]);
        }
    }

    int16_t v[] = {0, 0};
    my_arduino->send_to_mc(v);

    ros::spinOnce();
    usleep(period);
}

